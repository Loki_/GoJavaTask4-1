package Task_4_3;

/**
 * Created by Loki_ on 13.11.16.
 */
public class User {

    private String name;
    private int balance;
    private int monthsOfEmployment;
    private String companyName;
    private int salary;
    private String currency;

    public User(String name, int balance, int monthsOfEmployment, String companyName, int salary, String currency){
        this.name = name;
        this.balance = balance;
        this.monthsOfEmployment = monthsOfEmployment;
        this.companyName = companyName;
        this.salary = salary;
        this.currency = currency;
    }

    public String getName(){
        return this.name;
    }
    public void setName(String name){
        this.name = name;
    }

    public int getBalance(){
        return this.balance;
    }
    public void setBalance(int balance){
        this.balance = balance;
    }

    public int getMonthsOfEmployment(){
        return this.monthsOfEmployment;
    }
    public void setMonthsOfEmployment(int monthsOfEmployment){
        this.monthsOfEmployment = monthsOfEmployment;
    }

    public String getCompanyName(){
        return this.companyName;
    }
    public void setCompanyName(String companyName){
        this.companyName = companyName;
    }

    public int getSalary(){
        return this.salary;
    }
    public void setSalary(int salary){
        this.salary = salary;
    }

    public String getCurrency(){
        return this.currency;
    }
    public void setCurrency(String currency){
        this.currency = currency;
    }

    public void paySalary(){
        int sum = balance + salary;
    }
    public int withdraw(int summ){
        int cash;
        if (summ < 1000){
            cash = summ + (summ/100*5);
        } else {
            cash = summ + (summ/100*10);
        }
        int res = balance - cash;
        return res;
    }
    public int companyNameLenght(String companyName){
        int lenght = companyName.length();
        return lenght;
    }
    public int monthIncreaser(int addMonth){
        int summMonths = monthsOfEmployment + addMonth;
        return summMonths;
    }
}
