package Task_4_1;

import java.util.Currency;

/**
 * Created by Loki_ on 17-Nov-16.
 */
public abstract class Bank {
    long id;
    String bankCountry;
    Currency currency;
    int numberOfEmployees;
    double avrSalaryOfEmployee;
    long rating;
    long totalCapital;

    public Bank(long id, String bankCountry, Currency currency, int numberOfEmployees, double avrSalaryOfEmployee, long rating, long totalCapital){
        this.id = id;
        this.bankCountry = bankCountry;
        this.currency = currency;
        this.numberOfEmployees = numberOfEmployees;
        this.avrSalaryOfEmployee = avrSalaryOfEmployee;
        this.rating = rating;
        this.totalCapital = totalCapital;
    }

    public long getId(){
        return this.id;
    }
    public void setId(long id){
        this.id = id;
    }
    public String getBankCountry(){
        return this.bankCountry;
    }
    public void setBankCountry(String bankCountry){
        this.bankCountry = bankCountry;
    }
    public Currency getCurrency(){
        return this.currency;
    }
    public void setCurrency(Currency currency){
        this.currency = currency;
    }
    public int getNumberOfEmployees(){
        return this.numberOfEmployees;
    }
    public void setNumberOfEmployees(int numberOfEmployees){
        this.avrSalaryOfEmployee = numberOfEmployees;
    }
    public double getAvrSalaryOfEmployee(){
        return this.avrSalaryOfEmployee;
    }
    public void setAvrSalaryOfEmployee(double avrSalaryOfEmployee){
        this.avrSalaryOfEmployee = avrSalaryOfEmployee;
    }
    public long getRating() {
        return this.rating;
    }
    public void setRating(long rating){
        this.rating = rating;
    }
    public long getTotalCapital() {
        return this.totalCapital;
    }
    public void setTotalCapital(long totalCapital) {
        this.totalCapital = totalCapital;
    }

    abstract int getLimitOfWithdrawal();
    abstract int getLimitOfFunding();
    abstract int getMonthlyRate();
    abstract int getCommission(int summ);

    double moneyPaidMonthlyForSalary() {
        double allSalary = numberOfEmployees * avrSalaryOfEmployee;
        return allSalary;
    }
}
